-- bootstrap lazy.nvim, LazyVim and your plugins
require("config.lazy")

-- Use tree mode for netrw
vim.g.netrw_liststyle = 3

-- Ctrl+arrow window navigation
vim.api.nvim_set_keymap("n", "<C-Up", ":wincmd k<CR>", {})
vim.api.nvim_set_keymap("n", "<C-Down", ":wincmd j<CR>", {})
vim.api.nvim_set_keymap("n", "<C-Left", ":wincmd h<CR>", {})
vim.api.nvim_set_keymap("n", "<C-Right", ":wincmd l<CR>", {})

-- Folds
vim.opt.foldmethod = "syntax"
vim.opt.foldlevelstart = 1

-- Use 24-bit (true-color) mode
vim.opt.termguicolors = true

-- Add line numbers
vim.opt.number = true

-- default space and tab handling
vim.opt.shiftwidth = 2
vim.opt.tabstop = 2
vim.opt.expandtab = true
vim.opt.softtabstop = 2
