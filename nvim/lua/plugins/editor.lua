return {
  "matze/vim-move",
  "tmhedberg/simpylfold",
  "mg979/vim-visual-multi",
  {
    "folke/todo-comments.nvim",
    config = true,
    opts = {
      keywords = {
        NOTE = { icon = "", color = "hint", alt = { "INFO" } },
      },
      -- TODO: Figure out highlight and search patterns that match `[NOTE]`
      highlight = {},
      search = {},
    },
  },
  {
    "dyng/ctrlsf.vim",
    config = function()
      vim.api.nvim_set_keymap("n", "<C-F>", ":CtrlSF", {})
      vim.g.ctrl_sf_ifnore_dir = { "public", "node_modules", "log" }
    end,
  },
  { "junegunn/fzf", build = "./install --bin" },
  {
    "junegunn/fzf.vim",
    config = function()
      -- table.insert(vim.opt.rtp, '~/.fzf')
      vim.api.nvim_set_keymap("n", "<C-t>", ":FZF .<CR>", { silent = true })
    end,
  },
  {
    "junegunn/limelight.vim",
    keys = {
      { "<C-h>", ":Limelight!!<CR>", desc = "Toggle focus on current block", silent = true },
    },
  },
  {
    "lewis6991/gitsigns.nvim",
    opts = {
      on_attach = function(buffer)
        local gs = package.loaded.gitsigns

        local function map(mode, l, r, desc)
          vim.keymap.set(mode, l, r, { buffer = buffer, desc = desc })
        end

        map("n", "]h", gs.next_hunk, "Next Hunk")
        map("n", "[h", gs.prev_hunk, "Prev Hunk")
        map("n", "<leader>hp", gs.preview_hunk, "Preview Hunk")
        map("n", "<leader>hb", function()
          gs.blame_line({ full = true })
        end, "Blame line with full commit message")
        map("n", "<leader>tb", gs.toggle_current_line_blame, "Toggle current line blame")
        map("n", "<leader>hd", gs.diffthis, "Diff this")
        map("n", "<leader>hD", function()
          gs.diffthis("~")
        end, "Diff this ~")
        map("n", "<leader>td", gs.toggle_deleted, "Toggle deleted")
      end,
    },
  },
}
