return {
  {
    "nvim-neotest/neotest",
    dependencies = {
      "nvim-neotest/neotest-jest",
    },
    opts = {
      adapters = {
        "neotest-jest",
        ["neotest-rspec"] = {
          -- Directories you don't want the adapter to search for `_spec.rb` files
          filter_dirs = { ".git", "node_modules", "app", "lib" },
          -- NOTE: By default neotest-rspec uses the system wide rspec gem instead of the one through bundler
          -- rspec_cmd = function()
          --   return vim.tbl_flatten({
          --     "bundle",
          --     "exec",
          --     "rspec",
          --   })
          -- end,
        },
      },
      discovery = {
        -- Disabling test discovery dramatically improves performance
        enabled = false,
      },
      status = { virtual_text = false },
      icons = {
        -- passed = '✔'
        -- passed = '',
        running = "🧪",
        passed = "✅",
        -- failed = '❎',
        failed = "",
      },
      output = { open_on_run = true },
    },
    -- stylua: ignore
    keys = {
      { "<leader>tt", function() require("neotest").run.run(vim.fn.expand("%")) end, desc = "Run File" },
      { "<leader>tr", function() require("neotest").run.run() end, desc = "Run Nearest" },
      { "<leader>ts", function() require("neotest").summary.toggle() end, desc = "Toggle Summary" },
      { "<leader>to", function() require("neotest").output.open({ enter = true, auto_close = true }) end, desc = "Show Output" },
      { "<leader>tO", function() require("neotest").output_panel.toggle() end, desc = "Toggle Output Panel" },
      { "<leader>tS", function() require("neotest").run.stop() end, desc = "Stop" },
    },
  },
}
