return {
  { "nvim-neo-tree/neo-tree.nvim", enabled = false },
  { "echasnovski/mini.ai", enabled = false },
  { "folke/persistence.nvim", enabled = false },
  { "echasnovski/mini.animate", enabled = false },
  { "akinsho/bufferline.nvim", enabled = false },
  { "nvim-telescope/telescope.nvim", enabled = false },
}
