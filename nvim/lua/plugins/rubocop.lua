return {
  {
    "neovim/nvim-lspconfig",
    opts = {
      ---@type lspconfig.options
      servers = {
        rubocop = {
          settings = {
            workingDirectory = { mode = "auto" },
          },
        },
      },
      setup = {
        rubocop = function()
          local formatter = require("lazyvim.util").lsp.formatter({
            name = "rubocop: lsp",
            primary = false,
            priority = 200,
            filter = "rubocop",
          })

          -- register the formatter with LazyVim
          require("lazyvim.util").format.register(formatter)
        end,
      },
    },
  },
  {
    "stevearc/conform.nvim",
    opts = {
      formatters_by_ft = {
        ["ruby"] = { "rubocop" },
      },
    },
  },
}
