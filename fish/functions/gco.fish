function gco
  argparse 'd/date=' -- $argv
  if set -q _flag_date
    echo "Checking out last commit on" $_flag_date
    set dated_commit (git log --until=$_flag_date -n 1 --pretty=format:%H)
    git checkout -q $dated_commit
    git log -n 1
    return
  end
  git checkout $argv
end
