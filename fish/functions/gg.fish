function gg
# set branches_and_status (git for-each-ref --format '%(refname:short) %(upstream:track)' refs/heads)
# git for-each-ref --format '%(refname:short) %(upstream:track)' |
#   awk '$2 == "[gone]" {print $1}' |
#   xargs -r git branch -D
  git gone
end
