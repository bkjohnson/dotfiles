# The single argument should be a file path
function whose --argument-names 'filename'
  if not test $filename
    echo "Missing file argument"
    return 1
  end

  # heading "$argv"
  # echo
  heading "Commits by author:"
  git shortlog -n -s $filename

  heading \n"Lines in current version by author:"
  git ls-tree -r -z --name-only HEAD -- $filename | xargs -0 -n1 git blame --line-porcelain HEAD | sed -n 's/^author //p' | sort -nr | uniq -c
end

function heading
  echo (set_color --bold)$argv(set_color normal)
end
