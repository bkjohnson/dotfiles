function gd
    argparse a/ancestor -- $argv
    if set -q _flag_ancestor
        set default_branch (git symbolic-ref refs/remotes/origin/HEAD --short)
        git diff $default_branch...
    else
        git diff $argv
    end
end
