# Create a URL based on the current repo + filepath passed as an arg
# and open the URL in a browser.
#
# Assumptions:
#   - the git remote origin is ssh rather than http
function gshow
  set remote (git config --get remote.origin.url)
  set branch (git branch --show-current)
  set filepath $argv[1]

  # Process the remote to get the pieces of the final url
  set pieces (string split : $remote)
  set host (string sub --start 5 $pieces[1]) # skip the beginning `git@`
  set user_repo (string replace .git '' $pieces[2]) # remove the trailing `.git`

  # Construct the url & open it in the system default browser
  xdg-open (string join / https:/ $host $user_repo blob $branch $filepath)
end
