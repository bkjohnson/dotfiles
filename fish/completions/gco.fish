# Pieced together from:
# https://github.com/fish-shell/fish-shell/blob/8082f8c0569a1f8856496cd0abf42911bb85aa6b/share/completions/git.fish
function __fish_git_branches
    # This is much faster than using `git branch`,
    # and avoids having to deal with localized "detached HEAD" messages.
    __fish_git for-each-ref --format='%(refname)' refs/heads/ refs/remotes/ 2>/dev/null \
        | string replace -r '^refs/heads/(.*)$' '$1\tLocal Branch' \
        | string replace -r '^refs/remotes/(.*)$' '$1\tRemote Branch'
end

complete -k -f -c gco -n 'not contains -- -- (commandline -opc)' -a '(__fish_git_recent_commits --all)'
complete -k -f -c gco -n 'not contains -- -- (commandline -opc)' -a '(__fish_git_tags)' -d Tag
complete -k -f -c gco -n 'not contains -- -- (commandline -opc)' -a '(__fish_git_unique_remote_branches)' -d 'Unique Remote Branch'
complete -k -f -c gco -n 'not contains -- -- (commandline -opc)' -a '(__fish_git_branches)'
complete -k -f -c gco -n 'not contains -- -- (commandline -opc)' -a '(__fish_git_heads)' -d Head
complete -k -f -c gco -n 'not contains -- -- (commandline -opc)' -a '(__fish_git_local_branches)'
complete -f -c gco -s b -d 'Create a new branch'
complete -f -c gco -s B -d 'Create a new branch or reset existing to start point'
complete -f -c gco -s t -l track -d 'Track a new branch'
complete -f -c gco -l theirs -d 'Keep staged changes'
complete -f -c gco -l ours -d 'Keep unmerged changes'
complete -f -c gco -l recurse-submodules -d 'Update the work trees of submodules'
complete -f -c gco -l no-recurse-submodules -d 'Do not update the work trees of submodules'
complete -f -c gco -l progress -d 'Report progress even if not connected to a terminal'
complete -f -c gco -l no-progress -d "Don't report progress"
complete -f -c gco -s f -l force -d 'Switch even if working tree differs or unmerged files exist'
