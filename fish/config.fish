
if not functions -q fisher
    set -q XDG_CONFIG_HOME; or set XDG_CONFIG_HOME ~/.config
    curl https://git.io/fisher --create-dirs -sLo $XDG_CONFIG_HOME/fish/functions/fisher.fish
    fish -c fisher
end

if not type -q delta
    echo Installing git-delta
    wget -q https://github.com/dandavison/delta/releases/latest/download/git-delta_0.18.2_amd64.deb
    sudo dpkg -i git-delta_0.18.2_amd64.deb
    rm git-delta_0.18.2_amd64.deb
end

export VISUAL=nvim
export EDITOR="$VISUAL"
export AWS_VAULT_PROMPT=ykman
# Relevant issue: https://github.com/DataDog/dd-trace-rb/issues/3084
export DD_TRACE_STARTUP_LOGS=false
export PATH="$HOME/.rbenv/bin:$PATH"
export PATH="/home/linuxbrew/.linuxbrew/bin:$PATH"
status --is-interactive; and rbenv init - fish | source

# Colorscheme: Bay Cruise
set -U fish_color_normal normal
set -U fish_color_command 009999
set -U fish_color_quote 5CCCCC
set -U fish_color_redirection BF7130
set -U fish_color_end FFB273
set -U fish_color_error FF7400
set -U fish_color_param 33CCCC
set -U fish_color_comment FF9640
set -U fish_color_match --background=brblue
set -U fish_color_selection white --bold --background=brblack
set -U fish_color_search_match bryellow --background=brblack
set -U fish_color_history_current --bold
set -U fish_color_operator 00a6b2
set -U fish_color_escape 00a6b2
set -U fish_color_cwd green
set -U fish_color_cwd_root red
set -U fish_color_valid_path --underline
set -U fish_color_autosuggestion 006363
set -U fish_color_user brgreen
set -U fish_color_host normal
set -U fish_color_cancel --reverse
set -U fish_pager_color_prefix normal --bold --underline
set -U fish_pager_color_progress brwhite --background=cyan
set -U fish_pager_color_completion normal
set -U fish_pager_color_description B3A06D
set -U fish_pager_color_selected_background --background=brblack
set -U fish_pager_color_selected_prefix
set -U fish_pager_color_secondary_description
set -U fish_pager_color_selected_completion
set -U fish_color_keyword
set -U fish_color_option
set -U fish_pager_color_secondary_prefix
set -U fish_pager_color_selected_description
set -U fish_pager_color_secondary_completion
set -U fish_pager_color_secondary_background
set -U fish_color_host_remote
set -U fish_pager_color_background

# pnpm
set -gx PNPM_HOME "/home/brooks/.local/share/pnpm"
if not string match -q -- $PNPM_HOME $PATH
    set -gx PATH "$PNPM_HOME" $PATH
end

alias pn=pnpm
# pnpm end
