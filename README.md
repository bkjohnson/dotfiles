# dotfiles

## Manual steps

Some of the not-yet-automated parts of the setup are:

1. Installing regolith
1. Installing git
1. Configuring firefox with vertical tabs
   - [Create a userChrome.css file][1]

[1]: https://web.archive.org/web/20240910152946/https://www.userchrome.org/how-create-userchrome-css.html
