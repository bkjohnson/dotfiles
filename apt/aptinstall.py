#!/usr/bin/env python3

import os
import yaml
from subprocess import run

dir_path = os.path.dirname(os.path.realpath(__file__))
package_file = open(f"{dir_path}/packages.yml", "r")
packages = yaml.load(package_file, Loader=yaml.FullLoader)

for package in packages:
    result = run(["which", package])
    if result.returncode == 1:
        run(["sudo", "apt", "install", package, "-y"])
    else:
        print(f"Already installed {package}")
